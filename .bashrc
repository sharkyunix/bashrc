# custom .bashrc

# flag to change between powerline and normal mode. set 1 to enable powerline
POWERLINE=1

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

######################
# EFFECTS AND COLORS #
######################

#text effect
te()
{
    case "$1" in
        bold)       printf  "\001\e[1m\002";;
        blinking)   printf  "\001\e[5m\002";;
    esac
}

# foreground color
fgc()
{
    case "$1" in
        black)      printf  "\001\e[30m\002";;
        red)        printf  "\001\e[31m\002";;
        green)      printf  "\001\e[32m\002";;
        yellow)     printf  "\001\e[33m\002";;
        blue)       printf  "\001\e[34m\002";;
        magenta)    printf  "\001\e[35m\002";;
        cyan)       printf  "\001\e[36m\002";;
        white)      printf  "\001\e[37m\002";;
        orange)     printf 38\;5\;166;;
    esac
}

# background color
bgc()
{
    case "$1" in
        black)      printf  "\001\e[40m\002";;
        red)        printf  "\001\e[41m\002";;
        green)      printf  "\001\e[42m\002";;
        yellow)     printf  "\001\e[43m\002";;
        blue)       printf  "\001\e[44m\002";;
        magenta)    printf  "\001\e[45m\002";;
        cyan)       printf  "\001\e[46m\002";;
        white)      printf  "\001\e[47m\002";;
        orange)     printf 48\;5\;166;;
    esac;
}

# reset colors
reset()
{
	printf "\001\e[0m\002"
}

##############
# INDICATORS #
##############

# verify for background jobs
bg_job()
{
	if [[ $(jobs | wc -l) -ne 0 ]]; then
		printf " \001\e[33m\002B"
	fi
}

# VIM shell variable detector
vv()
{
	if [[ ! -z $VIM ]]; then
		printf " \001\e[32m\002V"
	fi
}

#########################
# GIT REPOSITORY STATUS #
#########################

# detect actual branch and if work tree is dirty
branch()
{
	CLEAN="nothing to commit, working tree clean"
	if [[ $(git status 2> /dev/null | tail -n1) != $CLEAN ]]; then 
		printf " \001\e[31m\002$(git branch 2> /dev/null | grep '^*' | colrm 1 2)"
	else
		printf " \001\e[32m\002$(git branch 2> /dev/null | grep '^*' | colrm 1 2)"
	fi
}

# untrack files counter
untrack()
{
	GITS=$(git status 2> /dev/null | sed -n '/Untracked/,/^$/p' | wc -l)
	if [[ $GITS -ne 0 ]]; then
		UNTRACK=$(echo "$GITS - 3" | bc)
		printf " \001\e[33m\002+$UNTRACK"
	fi
}

# deleted files counter
deleted()
{
	DEL=$(git status 2> /dev/null | grep deleted | wc -l)
	if [[ $DEL -ne 0 ]]; then
		printf " \001\e[31m\002-$DEL"
	fi
}

# modified files counter
modified()
{
	MOD=$(git status 2> /dev/null | grep modified | wc -l)
	if [[ $MOD -ne 0 ]]; then
		printf " \001\e[37m\002~$MOD"
	fi
}

# added files counter
add()
{
	ADD=$(git status 2> /dev/null | awk '/Changes to be committed/,/^$/{print;f=1} f&&/^$/{exit}' | wc -l)
	if [[ $ADD -ne 0 ]]; then
		TRACK=$(echo "$ADD - 3" | bc)
		printf " \001\e[32m\002^$TRACK"
	fi
}

######################
# CHECK IF I AM ROOT #
######################

uid_color()
{
	if [ $UID -eq 0 ]; then
		printf "$(reset)$(bgc red)$(fgc black)"
	else
		printf "$(reset)$(bgc blue)$(fgc black)"
	fi
}

uid_arrow()
{
	if [ $UID -eq 0 ]; then
		printf "$(reset)$(fgc red)$(reset)"
	else
		printf "$(reset)$(fgc blue)$(reset)"
	fi
}

##################
# DISPLAY PROMPT #
##################

if [ $POWERLINE -eq 1 ]; then
	export PS1='$(bgc black)$(te bold)$(bg_job)$(vv)\
$(te bold)$(branch)$(untrack)$(deleted)$(modified)$(add) $(uid_color) \w $(uid_arrow) '
else
	export PS1='\[\e[36m\][$(bg_job)$(vv) \[\e[35m\]\W$(branch)$(untrack)$(deleted)$(modified)$(add)\[\e[36m\]]\$\[\e[0m\] '
fi

bind "set completion-ignore-case on"

export EDITOR=vim
