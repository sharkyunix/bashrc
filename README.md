# Description

My bashrc configure. Its have two modes: powerline and normal mode, both with
support to display git information when inside a git repository

Git status indicator:

+: untracked files;

-: deleted files;

~: modified files;

^: added files;
